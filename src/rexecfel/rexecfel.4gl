SCHEMA data4gl

MAIN
DEFINE fac RECORD LIKE facturafel_e.*
DEFINE comando STRING


DEFINE dbname STRING    --BD que identifica a la sucursal
DEFINE dbcomp STRING    --BD @ instancia
DEFINE bodnum SMALLINT  --id de bodega que identifica a la sucursal
DEFINE numero STRING    --id del documento a certificar
 
    --CONNECT TO 'modula@desa_tcp' AS 'informix' USING 'informix'
    
    {SELECT * INTO fac.*
      FROM facturafel_e
    WHERE fac_id = 888--}
    LET dbname       = arg_val(1)
    LET bodnum       = arg_val(2)
    LET fac.fac_id   = arg_val(3)

    LET dbcomp = dbname CLIPPED, '@perco_tcp'
    --DISPLAY "Conexión ", fac.nombre_c 
        CASE
            WHEN fac.fac_id < 10      LET numero = fac.fac_id USING "#"
            WHEN fac.fac_id < 100     LET numero = fac.fac_id USING "##"
            WHEN fac.fac_id < 1000    LET numero = fac.fac_id USING "###"
            WHEN fac.fac_id < 10000   LET numero = fac.fac_id USING "####"
            WHEN fac.fac_id < 100000  LET numero = fac.fac_id USING "#####"
            WHEN fac.fac_id < 1000000 LET numero = fac.fac_id USING "######"
        END CASE
        
        DISPLAY 'SOLICITA EJECUTAR PARA EL FAC_ID: ', numero
        DISPLAY CURRENT HOUR TO FRACTION(3)
        --LET comando = 'ssh informix@192.168.26.133 /home/informix/runfel.sh fglrun ande_fel_g4s.42r ', numero CLIPPED, ' modula@desa_tcp 0 0'
        --LET comando = 'ssh informix@192.168.26.133 /home/informix/runfel.sh ', numero CLIPPED, ' modula@desa_tcp 0 0'
        
        -- Desarrollo 
        --LET comando = 'ssh informix@192.168.26.133 /home/informix/runfel.sh ', 888, ' modula@desa_tcp 0 0'
        --Perco 
        LET comando = "/opt/csw/bin/sshpass -p 'informix' ssh informix@192.168.1.108 /app/Fetiche/FETICHE.fel/bin/runfel.sh ", numero, dbcomp, bodnum
        DISPLAY comando 
        RUN comando
        DISPLAY CURRENT HOUR TO FRACTION(3)
    
END MAIN