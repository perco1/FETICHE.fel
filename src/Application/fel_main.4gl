# fel_main.4gl

IMPORT XML

SCHEMA pru
--DATABASE pru

GLOBALS "fel_globales.4gl"
GLOBALS "../Library/ws_g4s/ws_g4s_fel.inc"
GLOBALS "../Library/sat/sat_xml_docto.inc"
GLOBALS "../Library/sat/sat_xml_anula.inc"

MAIN

    CALL STARTLOG("ERR_ANDE")
    --CALL main_crea_log()

    LET id           = arg_val(1)
    LET fac_database = arg_val(2)
    LET bodnum       = arg_val(3)
    --LET fac_corr     = arg_val(3)
    --LET fac_pdf      = arg_val(4)
    LET ande_debug   = FGL_GETENV("ANDE_DEBUG")

    
    --LET id = 339 

    IF fac_database IS NOT NULL THEN
       CONNECT TO fac_database AS 'informix' USING 'losfitos'
       IF sqlca.sqlcode = 0 THEN  
          DISPLAY "Conectado a BD ", fac_database 
       ELSE 
          DISPLAY "Error al conectarse a BD ", fac_database
          RETURN 
       END IF  
    END IF    
    IF id > 0 THEN
            INITIALIZE factura.* TO NULL
            SELECT serie, num_doc, tipod, fecha, 
                    fecha_em, fecha_anul, num_aut, 
                    fecha_re, rango_i, rango_f, 
                    FROM, TO, cc, formats, 
                    tipo_doc, estado_doc, ant_serie, 
                    ant_numdoc, ant_fecemi, ant_resoluc, 
                    c_moneda, tipo_cambio, info_reg,
                    num_int, nit_e, nombre_c, idioma_e, 
                    nombre_e, codigo_e, dispositivo_e, 
                    direccion_e, departamento_e, municipio_e,
                    pais_e, codpos_e, nit_r, es_cui, 
                    nombre_r, idioma_r, direccion_r, 
                    departamento_r, municipio_r, pais_r, 
                    codpos_r, total_b, total_d, monto_d, 
                    total_i, ing_netosg, total_iva1, tipo1, 
                    base1, tasa1, monto1, total_neto, 
                    serie_e, numdoc_e, autorizacion, estatus, 
                    fac_id, p_descuento,
                    --mto_descuento decimal(14,2),
                    fel_msg, id_factura, total_en_letras, 
                    tipo_pago
                INTO factura.serie, factura.num_doc, factura.tipod, factura.fecha,
                    factura.fecha_em, factura.fecha_anul, factura.num_aut,
                    factura.fecha_re, factura.rango_i, factura.rango_f,
                    factura.FROM, factura.TO, factura.cc, factura.formats,
                    factura.tipo_doc, factura.estado_doc, factura.ant_serie,
                    factura.ant_numdoc, factura.ant_fecemi, factura.ant_resoluc,
                    factura.c_moneda, factura.tipo_cambio, factura.info_reg,
                    factura.num_int, factura.nit_e, factura.nombre_c, factura.idioma_e,
                    factura.nombre_e, factura.codigo_e, factura.dispositivo_e,
                    factura.direccion_e, factura.departamento_e, factura.municipio_e,
                    factura.pais_e, factura.codpos_e, factura.nit_r, factura.es_cui,
                    factura.nombre_r, factura.idioma_r, factura.direccion_r,
                    factura.departamento_r, factura.municipio_r, factura.pais_r,
                    factura.codpos_r, factura.total_b, factura.total_d, factura.monto_d,
                    factura.total_i, factura.ing_netosg, factura.total_iva1, factura.tipo1,
                    factura.base1, factura.tasa1, factura.monto1, factura.total_neto,
                    factura.serie_e, factura.numdoc_e, factura.autorizacion, factura.estatus,
                    factura.fac_id, factura.p_descuento, 
                    --mto_descuento decimal(14,2),
                    factura.fel_msg, factura.id_factura, factura.total_en_letras,
                    factura.tipo_pago
              FROM facturafel_e
             WHERE fac_id = id
               AND estatus = 'P'

            IF sqlca.sqlcode = 100 THEN
               CALL main_crea_log()
               LET fel.flag_error = 1
               LET fel.msg_error = "FEL 2.0 - Documento no tiene status P"
                    
               UPDATE factura_log SET factura_log.* = fel.*
                  WHERE @correlativo = fel.correlativo
               DISPLAY fel.msg_error
               RETURN 
            ELSE 
               IF sqlca.sqlcode < 0 THEN 
                  --CALL main_crea_log()
                  LET fel.flag_error = 1
                  LET fel.msg_error = "FEL 2.0 - Error SQL ", sqlca.sqlcode
                       
                  UPDATE factura_log SET factura_log.* = fel.*
                     WHERE @correlativo = fel.correlativo
                  DISPLAY fel.msg_error
                  RETURN
               ELSE 
                  IF ande_debug = 1 THEN
                     DISPLAY SFMT("La factura (fac_id #%1 tiene status %2.)", factura.fac_id,  factura.estatus)
                  END IF 
               END IF 
            END IF 

            IF factura.fac_id IS NOT NULL THEN

               INITIALIZE conexion.* TO NULL
               SELECT * INTO conexion.*
                 FROM empresas1
                 WHERE bodega = bodnum

               IF SQLCA.sqlcode <> NOTFOUND THEN
                  LET directorio = fel_valida_directorio(factura.fecha)
                  LET directorio2 = fel_valida_directorio2(factura.fecha)
                  IF (TODAY - factura.fecha) <= 4 THEN
                     LET fac_cant_det = 0
                     IF factura.estado_doc CLIPPED = 'ORIGINAL'  OR
                        factura.estado_doc CLIPPED = 'ANTIGUO' THEN
                        SELECT COUNT(*) INTO fac_cant_det
                          FROM facturafel_ed
                         WHERE fac_id = id
                         DISPLAY "antes del  fac_cant_det > 0============"   
                        IF fac_cant_det > 0 THEN
                           IF ande_debug = 1 THEN DISPLAY SFMT("Factura tiene %1 de detalle.", fac_cant_det) END IF 
                           DISPLAY "====Antes del fel_docto_build====="
                           CALL fel_docto_build()
                           IF ande_debug = 1 THEN DISPLAY "Despues del build_doc" END IF  
                           CALL main_envio_datos('R')
                           IF ande_debug = 1 THEN DISPLAY "Despues del envio" END IF
                        ELSE
                           DISPLAY "No se encontró detalle para la factura solicitada."
                        END IF
                     ELSE
                        IF factura.estado_doc CLIPPED = 'ANULADO' THEN
                           CALL fel_anula_docto()
                           CALL main_envio_datos('A')
                        ELSE
                           DISPLAY "No se reconoce el estado del documento."
                        END IF
                     END IF
                  ELSE
                       IF ande_debug = 1 THEN DISPLAY SFMT("Factura tiene fecha %1 son mas de 4 d�as.", factura.fecha) END IF 
                       --CALL main_crea_log()
                       LET fel.dir_arch_cer = directorio CLIPPED,'/cer_', fel.request_id CLIPPED,'.xml'
                       CALL main_envio_datos('B')
                  END IF
               ELSE
                   CALL main_crea_log()
                    LET fel.flag_error = 1
                    LET fel.msg_error = "FEL 2.0 (Archivo) ERROR : No se encontró datos de conexión al GFACE."
                    
                    UPDATE factura_log SET factura_log.* = fel.*
                    WHERE @correlativo = fel.correlativo
               END IF
            ELSE
                DISPLAY "NO se encontró el número de factura solicitado ", id, " o no tiene estatus 'P'"
            END IF
        ELSE
            DISPLAY "NO es un número de documento válido."
        END IF
    --ELSE
    --    DISPLAY "No se definió la base de datos a usar."
    --END IF
    
END MAIN

FUNCTION main_envio_datos(OPCION)
DEFINE opcion CHAR(1)
DEFINE mensaje    STRING
DEFINE resultado  SMALLINT

DISPLAY "OPCION lleva ---> ", OPCION
DISPLAY "fel.flag_error lleva ----> ", fel.flag_error
    IF fel.flag_error = 0 THEN
        CASE opcion
            WHEN 'R' -- REGISTRA
                CALL fel_gs4_send()
            WHEN 'A' -- REGISTRA
                CALL fel_gs4_anula()
            WHEN 'B' -- BUSCA FACTURA
                CALL traer_factura()
        END CASE
        IF fel.flag_error = 0 THEN
            LET fel.cod_archivo_cer = fel_archivo_xml(fel.dir_arch_cer)
        END IF
    END IF

    TRY
        IF fel.flag_error = 0 THEN
            UPDATE facturafel_e
               SET serie_e      = fel.certificador_serie, 
                   numdoc_e     = fel.certificador_numero, 
                   autorizacion = fel.sat_uuid,
                   fel_msg = fel.correlativo,
                   estatus = 'C'
               WHERE @fac_id = id
            DISPLAY "Proceso terminado satisfactoriamente."
        ELSE
            UPDATE facturafel_e
               SET fel_msg = fel.correlativo
             WHERE @fac_id = id
               AND numdoc_e = "0"
            DISPLAY "Proceso terminado con errores, revise el Log ID ", fel.correlativo 
        END IF
    CATCH
        LET mensaje = "FEL 2.0 (Archivo) ERROR : main_envio_datos ",STATUS||" ("||SQLCA.SQLERRM||")"
        LET resultado = FALSE
        LET fel.flag_error = 1
        LET fel.msg_error = mensaje
    END TRY
    
    UPDATE factura_log SET factura_log.* = fel.*
    WHERE @correlativo = fel.correlativo
END FUNCTION


FUNCTION main_crea_log()
DEFINE numero_acceso  DECIMAL(9,0)

    CALL STARTLOG("ERR_ANDE")

    INITIALIZE ns1GTDocumento.* TO NULL 

    LET fel.fac_id = id    
    LET fel.request_id = id USING "&&&&&&&&&&"
    LET fel.fecha_envio = CURRENT
    LET fel.correlativo = ultimo_corr_msg()
    LET fel.numero_acceso = numero_acceso
    LET fel.estatus = 1 -- ESTADO INICIAL
    LET fel.interna_tipod  = factura.tipod
    IF fel.interna_tipod IS NULL THEN LET fel.interna_tipod = "PEND" END IF 
    LET fel.interna_serie  = factura.serie
    IF fel.interna_serie IS NULL THEN LET fel.interna_serie = "PEND" END IF 
    LET fel.interna_numero = factura.num_doc
    IF fel.interna_numero IS NULL THEN LET fel.interna_numero = 0 END IF 
    LET fel.flag_error = 0

    INSERT INTO factura_log VALUES (fel.*)

END FUNCTION
{
--LET fel.estatus = 1 -- ESTADO INICIAL
--LET fel.estatus = 2 --TERMINÓ DE GRABAR INFORMACIÓN BÁSICA
--LET fel.estatus = 3 --TERMINÓ DE GRABAR ADENDAS Y COMPLEMENTOS
--LET fel.estatus = 4 --GENERÓ EL XML EN ARCHIVO ORIGINAL
--LET fel.estatus = 5 --GENERÓ EL XML FIRMADO
--LET fel.estatus = 6 -- LEVANTÓ EL ARCHIVO FIRMADO A MEMORIA
--LET fel.estatus = 7 -- Eviando el documento por SOAP
--LET fel.estatus = 8 -- RESPUESTA POSITIVA INICAINDO RECUPERAR DATOS
--LET fel.estatus = 9 -- Se levantó el XML recibido de G-FACE
--LET fel.estatus = 10 -- Se extrajo información del XML
--LET fel.estatus = 11 -- Ya se encuentra registrado el documeto
}

