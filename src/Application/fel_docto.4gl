IMPORT XML

SCHEMA pru  

GLOBALS "fel_globales.4gl"
GLOBALS "../Library/ws_g4s/ws_g4s_fel.inc"
GLOBALS "../Library/sat/sat_xml_docto.inc"
GLOBALS "../Library/sat/sat_xml_anula.inc"
GLOBALS "../Library/sat/sat_xml_fcam.inc"

FUNCTION fel_docto_build()
DEFINE nit            VARCHAR(25)
DEFINE frases         RECORD LIKE empresas1_f.*
DEFINE f              SMALLINT
DEFINE numero_acceso  DECIMAL(9,0)
DEFINE adenda         SMALLINT
DEFINE mensaje    STRING
DEFINE resultado  SMALLINT
DEFINE docto RECORD LIKE facturafel_e.*
DEFINE lbodega_afecta SMALLINT
DEFINE sql_stmt, dbname STRING

    CALL STARTLOG("ERR_ANDE")

    LET adenda = FALSE
    INITIALIZE ns1GTDocumento.* TO NULL 

    LET fel.fac_id = id    
    LET fel.request_id = id USING "&&&&&&&&&&"
    LET fel.fecha_envio = CURRENT
    LET fel.correlativo = ultimo_corr_msg()
    LET fel.numero_acceso = numero_acceso
    LET fel.estatus = 1 -- ESTADO INICIAL
    LET fel.interna_tipod  = factura.tipod
    LET fel.interna_serie  = factura.serie
    LET fel.interna_numero = factura.num_doc
    LET fel.flag_error = 0

    INSERT INTO factura_log VALUES (fel.*)

    TRY
        IF fel.estatus > 0 THEN
            ## -- DATOS BASICOS
            LET ns1GTDocumento.Version = 0.1
            LET ns1GTDocumento.SAT.ClaseDocumento = 'dte'
            LET ns1GTDocumento.SAT.DTE.ID = 'DatosCertificados'
            LET ns1GTDocumento.SAT.DTE.DatosEmision.ID = 'DatosEmision'
            
            ## DATOS DE EMISOR
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.AfiliacionIVA                = 'GEN'
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.CodigoEstablecimiento        = conexion.cod_sucursal CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.CorreoEmisor                 = factura.from CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.NITEmisor                    = utl_nit_singuion(conexion.nit)
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.NombreComercial              = factura.nombre_c CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.NombreEmisor                 = factura.nombre_e CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.DireccionEmisor.Direccion    = factura.direccion_e CLIPPED, ' ', conexion.direccion2
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.DireccionEmisor.CodigoPostal = factura.codpos_e CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.DireccionEmisor.Municipio    = factura.municipio_e CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.DireccionEmisor.Departamento = factura.departamento_e CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.DireccionEmisor.Pais         = 'GT'

            ## --DATOS GENERALES
            LET ns1GTDocumento.SAT.DTE.DatosEmision.DatosGenerales.FechaHoraEmision = factura.fecha_em CLIPPED --FHEmision
            LET ns1GTDocumento.SAT.DTE.DatosEmision.DatosGenerales.Tipo = factura.tipo_doc CLIPPED
            --LET ns1GTDocumento.SAT.DTE.DatosEmision.DatosGenerales.NumeroAcceso = numero_acceso
            
            # Datos Emision
            LET ns1GTDocumento.SAT.DTE.DatosEmision.DatosGenerales.CodigoMoneda = factura.c_moneda CLIPPED

            # Datos Receptor
            IF factura.tipo_doc <> 'NCRE' THEN 
               LET nit = utl_nit_singuion(factura.nit_r)
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.IDReceptor = factura.nit_r 
            
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.NombreReceptor = factura.nombre_r CLIPPED
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.Direccion = factura.direccion_r CLIPPED
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.Departamento = factura.departamento_r CLIPPED
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.Municipio    = factura.municipio_r CLIPPED
               IF factura.codpos_r IS NULL THEN
                   LET factura.codpos_r = 100
               END IF
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.CodigoPostal = factura.codpos_r CLIPPED
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.Pais = factura.pais_r CLIPPED
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.CorreoReceptor = factura.to CLIPPED
               
            ELSE
               ##################################################################################################################
               #-Una nota de credito puede afectar un documento emitido en otra tienda, 
               #-para este caso: Otra Tienda implica Otra Base de Datos, es decir que el id_factura se relaciona
               #-con un fac_id en otra base de datos, para ello en la tabla fac_nota se indica a que BD corresponde
               #-esa nota y en fac_bod se relacionan la bodega con el nombre de la base de datos
               #-----------------------------------------------------------------------------------------------------------------

               --1. Buscar serie, num_doc y id_factura en BD Local:facturafel_e donde fac_id = factura.fac_id
               --2. Buscar bodega_afecta en BD Local:fac_nota relacionando serie y num_doc con serie y docmto
               WHENEVER ERROR CONTINUE 
               SELECT   fac_nota.bodega_afecta 
                  INTO  lbodega_afecta
                  FROM  facturafel_e, fac_nota 
                  WHERE facturafel_e.serie     = fac_nota.serie
                  AND   facturafel_e.num_doc   = fac_nota.docmto 
                  AND   facturafel_e.fac_id    = factura.fac_id
               WHENEVER ERROR STOP 
               --3. Si bodega_afecta es NULL la busca localmente
               -->  Crea un sql_stmt para encontrar en facturafel_e los datos del documento que afecta donde fac_id = id_factura
               IF lbodega_afecta IS NULL OR lbodega_afecta = 0 THEN 
                  LET sql_stmt = "SELECT * ", --INTO docto.*
                                 " FROM facturafel_e ",
                                 " WHERE fac_id = ", factura.id_factura
               ELSE
                  --   Si bodega_afecta es diferente de NULL busca el nombre de la BD donde está el documento a afectar
                  --   Campo codigo en BD Local:fac_bod donde bodega = sea igual a bodega_afecta
                  --4. Crea un sql_stmt agregando el nombre de la BD:facturafel_e donde fac_id = id_factura
                  SELECT codigo INTO dbname FROM fac_bod WHERE bodega = lbodega_afecta
                  LET sql_stmt = "SELECT * ", --INTO docto.*
                                 " FROM ", dbname CLIPPED, ":facturafel_e ",
                                 " WHERE fac_id = ", factura.id_factura 
               END IF 
         
               --5. Realiza un prepare y execute into la sql_stmt armada anteriormente
               PREPARE ex_stmt FROM sql_stmt
               EXECUTE ex_stmt INTO docto.*
         
               LET nit = utl_nit_singuion(docto.nit_r)
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.IDReceptor = docto.nit_r 
            
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.NombreReceptor = docto.nombre_r CLIPPED
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.Direccion = docto.direccion_r CLIPPED
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.Departamento = docto.departamento_r CLIPPED
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.Municipio    = docto.municipio_r CLIPPED
               IF docto.codpos_r IS NULL THEN
                   LET docto.codpos_r = 100
               END IF
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.CodigoPostal = docto.codpos_r CLIPPED
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.Pais = docto.pais_r CLIPPED
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.CorreoReceptor = docto.to CLIPPED
                     
            END IF
            ## DETALLE FACTURA
            CALL fel_factura_items()

            LET fel.estatus = 2 --TERMINÓ DE GRABAR INFORMACIÓN BÁSICA

            ## ADENDA Y COMPLEMENTOS
            CASE factura.tipo_doc
                WHEN "FACT" --Fetiche
                    DECLARE cur_frasesfact CURSOR FOR
                        SELECT *
                          FROM empresas1_f
                          WHERE tipo_frase = 1
                    LET f = 0
                    FOREACH cur_frasesfact INTO frases.*
                         LET f = f + 1
                         LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].CodigoEscenario = frases.codigo_escenario
                         LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].TipoFrase = frases.tipo_frase
                    END FOREACH
                    DISPLAY "Antes de adenda"
                    CALL fel_adenada_fact()
                    DISPLAY "Despues de adenda"
                    {LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.NombreComplemento = 'FACTURA'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.URIComplemento = 'http://www.sat.gob.gt/dte/fel/CompCambiaria/0.1.0'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento._ANY_0 = fel_complento_FCAM()}
                    
                WHEN "FCAM" --PERCO
                    DECLARE cur_frases CURSOR FOR
                        SELECT *
                          FROM empresas1_f
                          WHERE tipo_frase IN (1,2)
                          
                    LET f = 0
                    FOREACH cur_frases INTO frases.*
                         LET f = f + 1
                         LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].CodigoEscenario = frases.codigo_escenario
                         LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].TipoFrase = frases.tipo_frase
                    END FOREACH
                    CALL fel_adenada_fcam()
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.NombreComplemento = 'FACTURA_CAMBIARIA'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.URIComplemento = 'http://www.sat.gob.gt/dte/fel/CompCambiaria/0.1.0'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento._ANY_0 = fel_complento_FCAM()
                    
                WHEN "NCRE" 
                    CALL fel_adenada_nota()
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.NombreComplemento = 'NOTA_CREDITO'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.URIComplemento = 'http://www.sat.gob.gt/face2/ComplementoReferenciaNota/0.1.0'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento._ANY_0 = fel_complento_nota()
                WHEN "NDEB" 
                    CALL fel_adenada_nota()
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.NombreComplemento = 'NOTA_DEBITO'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.URIComplemento = 'http://www.sat.gob.gt/face2/ComplementoReferenciaNota/0.1.0'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento._ANY_0 = fel_complento_nota()
                WHEN "FESP" 
                  IF bodnum = 1 THEN -- PERCO
                     DECLARE cur_frasesfespp CURSOR FOR
                        SELECT *
                          FROM empresas1_f
                          WHERE tipo_frase = 5
                          
                        LET f = 0
                        FOREACH cur_frasesfespp INTO frases.*
                           LET f = f + 1
                           LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].CodigoEscenario = frases.codigo_escenario
                           LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].TipoFrase = frases.tipo_frase
                        END FOREACH
                  ELSE --Fetiche
                     DECLARE cur_frasesfespf CURSOR FOR
                        SELECT *
                          FROM empresas1_f
                          WHERE tipo_frase = 5
                    LET f = 0
                    FOREACH cur_frasesfespf INTO frases.*
                         LET f = f + 1
                         LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].CodigoEscenario = frases.codigo_escenario
                         LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].TipoFrase = frases.tipo_frase
                    END FOREACH
                  END IF  
                    
                    IF factura.ES_CUI = 1 THEN
                        LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.TipoEspecial = 'CUI'
                    END IF 
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.NombreComplemento = 'FACTURA_ESPECIAL'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.URIComplemento = 'http://www.sat.gob.gt/face2/ComplementoFacturaEspecial/0.1.0'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento._ANY_0 = fel_complemento_FESP()
                    CALL fel_adenada_FESP()
            END CASE

            LET fel.estatus = 3 --TERMINÓ DE GRABAR ADENDAS Y COMPLEMENTOS
            IF ande_debug = 1 THEN DISPLAY SFMT("Proceso en estado %1", fel.estatus) END IF 
            CALL fel_arch_original()

        END IF
    CATCH
        LET mensaje = "FEL 2.0 (Archivo) ERROR :",STATUS||" ("||SQLCA.SQLERRM||")"
        LET resultado = FALSE
        LET fel.flag_error = 1
        LET fel.msg_error = mensaje
    END TRY

END FUNCTION


PRIVATE FUNCTION fel_factura_items()
DEFINE det      RECORD LIKE facturafel_ed.*
DEFINE i        SMALLINT
DEFINE TotalItem, ImpuestoItem DECIMAL(18,6)
DEFINE GranTotal, TotalImpuesto DECIMAL(18,6)
DEFINE mensaje    STRING
DEFINE resultado  SMALLINT

     CALL STARTLOG("ERR_ANDE")
     
    INITIALIZE det.* TO NULL
    LET i = 0
    LET GranTotal = 0
    LET TotalImpuesto = 0

   TRY
   DECLARE cur_detart CURSOR FOR
     SELECT *
       FROM facturafel_ed
       WHERE fac_id = factura.fac_id

   CALL detcods.CLEAR()
   FOREACH cur_detart INTO det.*
        IF det.cantidad < 0 THEN LET g_haycertificado = 1 CONTINUE FOREACH END IF  
        LET i = i + 1
        LET TotalItem = 0
        LET ImpuestoItem = 0 

        LET detcods[i].codigo = det.codigo_p

        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].BienOServicio  = det.categoria[1,1]
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].NumeroLinea    = i
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Cantidad       = det.cantidad
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].UnidadMedida   = det.unidad_m
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Descripcion    = det.descrip_p
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].PrecioUnitario = det.precio_u
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Precio         = det.precio_t
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Descuento      = det.descto_t
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Total          = det.base + det.monto

        # Cuando la descripci�n es larga
        {IF det.txt_especial = "S" THEN 
           LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Descripcion = det.txt_largo
        END IF} 

        LET TotalItem    =  det.base + det.monto
        LET ImpuestoItem = 0
        IF factura.tipo_doc <> 'NABN' THEN
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Impuestos.Impuesto[1].CodigoUnidadGravable = 1
                
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Impuestos.Impuesto[1].NombreCorto   = det.tipo
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Impuestos.Impuesto[1].MontoGravable = det.base
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Impuestos.Impuesto[1].MontoImpuesto = det.monto
        END IF

   END FOREACH
   CLOSE cur_detart

    IF factura.tipo_doc <> 'NABN' THEN
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Totales.TotalImpuestos.TotalImpuesto[1].NombreCorto = 'IVA'
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Totales.TotalImpuestos.TotalImpuesto[1].TotalMontoImpuesto = factura.monto1
    END IF
    LET ns1GTDocumento.SAT.DTE.DatosEmision.Totales.GranTotal = factura.total_neto
   CATCH
        LET mensaje = "FEL 2.0 (Archivo) ERROR :",STATUS||" ("||SQLCA.SQLERRM||")"
        LET resultado = FALSE
        LET fel.flag_error = 1
        LET fel.msg_error = mensaje
   END TRY
END FUNCTION

PRIVATE FUNCTION fel_arch_original()
DEFINE writertFEL xml.StaxWriter
DEFINE mensaje    STRING
DEFINE resultado  SMALLINT
DEFINE wsstatus   INTEGER
DEFINE retryAuth  INTEGER
DEFINE retryProxy INTEGER
DEFINE retry      INTEGER
DEFINE comando    STRING
DEFINE docXML     xml.DomDocument
DEFINE docXMLT    xml.DomDocument
DEFINE root       xml.DomNode
DEFINE archtmp    STRING
  CALL STARTLOG("ERR_ANDE")

  LET wsstatus = -1
  LET retryAuth = FALSE
  LET retryProxy = FALSE
  LET retry = TRUE
  LET fel.dir_arch_org = directorio2 CLIPPED,'/original/ori_', conexion.bodega USING "&&", "_", fel.request_id CLIPPED,'.xml'
  LET fel.dir_arch_fir = directorio2 CLIPPED,'/firmado/fir_', conexion.bodega USING "&&", "_", fel.request_id CLIPPED,'.xml'
  LET fel.dir_arch_cer = directorio CLIPPED,'/cer_', conexion.bodega USING "&&", "_", fel.request_id CLIPPED,'.xml'

   LET comando = "echo ", "dir_arch_original ", fel.dir_arch_org, ">> ande_cert.log"
   RUN comando 
   LET comando = "echo ", "dir_arch_fir ", fel.dir_arch_fir, ">> ande_cert.log"
   RUN comando 
   LET comando = "echo ", "dir_arch_cer ", fel.dir_arch_cer, ">> ande_cert.log"
   RUN comando 
   IF ande_debug = 1 THEN DISPLAY SFMT("dir_arch_original %1, dir_arch_fir %2, dir_arch_cer %3", fel.dir_arch_org, fel.dir_arch_fir, fel.dir_arch_cer) END IF  
   --DISPLAY "dir_arch_fir ", fel.dir_arch_fir 
   --DISPLAY "dir_arch_cer ", fel.dir_arch_cer
   --DISPLAY "dir_arch_original ", fel.dir_arch_org 
   --DISPLAY "dir_arch_fir ", fel.dir_arch_fir 
   --DISPLAY "dir_arch_cer ", fel.dir_arch_cer 
  
  LET resultado = FALSE
  TRY
      LET archtmp = fel_archivo_genera()
      LET writertFEL = xml.StaxWriter.Create()
      CALL writertFEL.setFeature("format-pretty-print",TRUE)
      CALL writertFEL.writeTo(archtmp) --fel.dir_arch_org)
      CALL writertFEL.startDocument("utf-8","1.0",FALSE)
      CALL xml.Serializer.VariableToStax(ns1GTDocumento,writertFEL)
      CALL writertFEL.endDocument()
      CALL writertFEL.CLOSE()
      
      LET docXMLT = xml.DomDocument.CREATE()
      CALL docXMLT.load(archtmp)
      LET root = docXMLT.getFirstDocumentNode()
      CALL docXMLT.declareNamespace(root,'cex', 'http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
      CALL docXMLT.declareNamespace(root,'cfe', 'http://www.sat.gob.gt/face2/ComplementoFacturaEspecial/0.1.0')
      CALL docXMLT.declareNamespace(root,'ds', 'http://www.w3.org/2000/09/xmldsig#')
      CALL docXMLT.declareNamespace(root,'cno', 'http://www.sat.gob.gt/face2/ComplementoReferenciaNota/0.1.0')
      CALL docXMLT.declareNamespace(root,'cfc', 'http://www.sat.gob.gt/dte/fel/CompCambiaria/0.1.0')
      --DISPLAY root.toString()
      
      CALL docXMLT.save(fel.dir_arch_org)
      
      CALL fel_archivo_elimina(archtmp)
      
      LET fel.estatus = 4 --GENERÓ EL XML EN ARCHIVO ORIGINAL
      
      --LET comando = '/opt/csw/java/jdk/jdk8/bin/java -jar FirmaDocumentos-1.0.jar ',

      --Corrigiendo adenda de correos
      LET comando = './corrigeAdendaMail ', fel.dir_arch_org
      RUN comando
      
      LET comando = 'java -jar NewFirmaDocumentos-1.0.jar ', 
                    fel.dir_arch_org CLIPPED,' ', 
                    conexion.llave_archivo CLIPPED,' ',
                    conexion.llave_pass CLIPPED,' ', 
                    fel.dir_arch_fir CLIPPED, 
                    " DatosEmision ", " http://www.sat.gob.gt/dte/fel/0.2.0"
      DISPLAY comando 
      RUN comando

      LET fel.estatus = 5 --GENERÓ EL XML FIRMADO

      LET resultado = TRUE
      LET docXML = xml.DomDocument.CREATE()
      
      CALL docXML.load(fel.dir_arch_fir)
      
      LET fel.xml_docto = docXML.saveToString()
      LET fel.estatus = 6 -- LEVANTÓ EL ARCHIVO FIRMADO A MEMORIA
  CATCH
    LET mensaje = "FEL 2.0 (Archivo) ERROR fel_arch_original:",STATUS||" ("||SQLCA.SQLERRM||")"
    LET resultado = FALSE
    LET fel.flag_error = 1
    LET fel.msg_error = mensaje
  END TRY

   LET comando = "chmod 777 ", fel.dir_arch_org
   RUN comando 
   LET comando = "chmod 777 ", fel.dir_arch_fir
   RUN comando
  DISPLAY "comando permisos fir ",comando  
   LET comando = "chmod 777 ", fel.dir_arch_cer 
   RUN comando
  
END FUNCTION
